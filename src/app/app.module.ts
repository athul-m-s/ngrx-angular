import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { StoreModule } from '@ngrx/store';
import { galleryReducer } from './store/gallery.reducer';
import { HttpClientModule } from '@angular/common/http';
import { GalleryService } from './gallery/gallery.service';
import { GalleryComponent } from './gallery/gallery.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { GalleryEffect } from './gallery/gallery.effect';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [AppComponent, GalleryComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    StoreModule.forRoot({ gallery: galleryReducer }),
    EffectsModule.forRoot([GalleryEffect]),
  ],
  providers: [GalleryService],
  bootstrap: [AppComponent],
})
export class AppModule {}
