import { createSelector } from '@ngrx/store';
import { GalleryModel } from '../gallery/gallery.model';

import { AppState } from './app.state';

export const galleryRootSelector = (state: AppState) => state.gallery;

export const uniqueAlbumIds = createSelector(
  galleryRootSelector,
  (gallery: GalleryModel[]) => {
    return [...new Set(gallery.map((photo) => photo.albumId))];
  }
);

export const albumCollectionByAlbumId = (albumId: number) =>
  createSelector(galleryRootSelector, (gallery: GalleryModel[]) => {
    if (albumId == -1) {
      return gallery;
    }
    return gallery.filter((photo) => photo.albumId == albumId);
  });
