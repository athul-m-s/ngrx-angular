import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { GalleryService } from './gallery.service';
import {
  invokeGalleryAPI,
  retrievedGalleryList,
} from '../store/gallery.action';
import {
  albumCollectionByAlbumId,
  uniqueAlbumIds,
} from '../store/gallery.selector';
import { GalleryModel } from './gallery.model';
import { pipe } from 'rxjs';

@Component({
  templateUrl: './gallery.component.html',
  selector: 'gallery',
})
export class GalleryComponent implements OnInit {
  albumSelectedId = -1;
  albumIds$ = this.store.pipe(select(uniqueAlbumIds));
  allGallery$ = this.store.pipe(
    select(albumCollectionByAlbumId(this.albumSelectedId))
  );
  constructor(
    private store: Store<{ gallery: GalleryModel[] }>,
    private galleryService: GalleryService
  ) {}

  ngOnInit(): void {
    // this.galleryService.loadGallery().subscribe((gallery) => {
    //   console.log(gallery);
    //   this.store.dispatch(
    //     retrievedGalleryList({ allGallery: gallery as GalleryModel[] })
    //   );
    // });
    this.store.dispatch(invokeGalleryAPI());
  }

  albumChange(event: number) {
    this.allGallery$ = this.store.pipe(select(albumCollectionByAlbumId(event)));
  }
}
